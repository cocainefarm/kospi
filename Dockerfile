FROM python:3.8-slim

RUN apt-get update && apt-get install -yq git

RUN pip install gunicorn

COPY . /kospi
WORKDIR /kospi
RUN pip install -r /kospi/requirements.txt && \
    pip install .

EXPOSE 8000

CMD ["gunicorn", "--bind=0.0.0.0", "kospi:app"]

