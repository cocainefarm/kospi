#!/usr/bin/env python3

from flask import Flask, render_template

from .config import get_config

app = Flask(__name__)

get_config()
app.config.from_object("kospi.config.Config")

from .db import db

from .pages.admin import admin_service, admin_settings, admin_users
from .pages.instance import instance_list, instance_create, instance_edit
