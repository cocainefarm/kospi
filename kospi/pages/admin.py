#!/usr/bin/env python3


from flask import render_template, request, redirect, url_for
from uuid import uuid4
from bson import binary

import pprint

pp = pprint.PrettyPrinter(indent=4)

from kospi import db, app
from kospi.k8s import get_custom_resources


@app.route("/service")
def admin_service():
    services = list(db.service.find())
    return render_template("admin/service_list.html.jinja", services=services)


@app.route("/service/create")
def admin_service_create():
    crds = get_custom_resources()
    return render_template("admin/service_create.html.jinja", crds=crds)


@app.route("/service/edit/<uuid:uuid>")
def admin_service_edit(uuid):
    return redirect(url_for("admin_service"))


@app.route("/service/delete/<uuid:uuid>")
def admin_service_delete(uuid):
    res = db.service.delete_one({"metadata.uuid": uuid})
    return redirect(url_for("admin_service"))


@app.route("/service/create/<name>/<version>", methods=["GET", "POST"])
def admin_service_create_crd(name, version):
    crd = get_custom_resources(name)
    if request.method == "GET":
        spec = next(filter(lambda ver: ver.name == version, crd.spec.versions), None)
        tmpl = render_form(spec.schema.open_apiv3_schema.properties["spec"].properties)
        return render_template(
            "admin/service_create_crd.html.jinja",
            crd=crd,
            spec=spec,
            tmpl=tmpl,
            name=name,
            version=version,
        )
    elif request.method == "POST":
        crd = crd.to_dict()
        del crd["metadata"]["annotations"][
            "kubectl.kubernetes.io/last-applied-configuration"
        ]
        payload = parse_form(request.form)
        spec = next(
            filter(lambda ver: ver["name"] == version, crd["spec"]["versions"]), None
        )
        spec["schema"]["open_apiv3_schema"]["properties"]["spec"][
            "properties"
        ] = merge_with_spec(
            spec["schema"]["open_apiv3_schema"]["properties"]["spec"]["properties"],
            payload["payload"],
        )
        metadata = {
            "ref": name,
            "version": version,
            "uuid": uuid4(),
            "name": payload["metadata"]["name"],
            "icon": payload["metadata"]["icon"],
            "description": payload["metadata"]["description"],
        }
        service = {
            "metadata": metadata,
            "spec": crd,
        }

        db.service.insert_one(service)
        return redirect(url_for("admin_service"))


def access_deep(value, keys):
    res = None
    for key in keys:
        res = value[key]
    return res


def merge_with_spec(spec, payload):
    base = spec
    for key, value in base.items():
        if value["type"] == "object":
            merge_with_spec(spec[key]["properties"], payload[key])
        else:
            if value["type"] == "boolean":
                value["default"] = payload[key].get("default", False)
            elif value["type"] == "array":
                value["default"] = payload[key].get("default", [])
            else:
                value["default"] = payload[key]["default"]
            value["enabled"] = payload[key].get("enabled", False)
    return base


def parse_form(data):
    res = {}
    for (key, value) in data.items():
        path = key.split(".")
        tmp = res
        for i, node in enumerate(path):
            if i == len(path) - 1:
                if value == "superuniqueboolvalue":
                    value = True
                tmp[node] = value
            else:
                if node not in tmp:
                    tmp[node] = {}
            tmp = tmp[node]
    return res


def render_form(spec, path="payload"):
    if spec is None:
        print("no item found {}".format(spec))
        return ""

    tmpl = ""
    for (key, value) in spec.items():
        tmpl += """
        <div class="box">
            <div class="field">
        """
        if value.type == "string":
            tmpl += """
            <div class="control">
                <label class="label">{name}</label>
                <input class="input" type="text" name="{id}.default">
                <p class="help">{help}</p>
            </div>

            <div class="control">
                <label class="label">
                    <input type="checkbox" name="{id}.enabled" checked value="superuniqueboolvalue"> Enabled
                </label>
            </div>
            """.format(
                name=key, id=joins(path, key), help=value.description
            )
        elif value.type == "integer":
            tmpl += """
            <div class="control">
                <label class="label">{name}</label>
                <input class="input" type="number" name="{id}.default">
                <p class="help">{help}</p>
            </div>
            <div class="control">
                <label class="label">
                    <input type="checkbox" name="{id}.enabled" checked value="superuniqueboolvalue"> Enabled
                </label>
            </div>
            """.format(
                name=key, id=joins(path, key), help=value.description
            )
        elif value.type == "boolean":
            tmpl += """
            <div class="control">
                <label class="label">
                <input type="checkbox" name="{id}.default" value="superuniqueboolvalue">
                {name}</label>
                <p class="help">{help}</p>
            </div>

            <div class="control">
                <label class="label">
                    <input type="checkbox" name="{id}.enabled" checked value="superuniqueboolvalue"> Enabled
                </label>
            </div>
            """.format(
                name=key, id=joins(path, key), help=value.description
            )
        elif value.type == "object":
            tmpl += """<label class="label">{}</label>""".format(key)
            tmpl += render_form(value.properties, joins(path, key))
        elif value.type == "array":
            tmpl += """<label class="label">{}</label>""".format(key)
            tmpl += render_form_array(key, value)
            tmpl += """
            <div class="control">
                <label class="label">
                    <input type="checkbox" name="{id}.enabled" checked value="superuniqueboolvalue"> Enabled
                </label>
            </div>
            """.format(
                id=joins(path, key)
            )
        tmpl += """  </div>
        </div>
        """
    return tmpl


def joins(*argv):
    res = ""
    for arg in argv:
        if arg.startswith("."):
            res += arg[1:] + "."
        elif arg.endswith("."):
            res += arg
        else:
            res += arg + "."
    if res.endswith("."):
        res = res[:-1]
    if res.startswith("."):
        res = res[1:]

    return res


def render_form_array(name, spec):
    if "enum" in spec.items:
        tmpl = """<div class="select is-multiple">
        <select multiple size="8" name="{id}.default" id="{id}">
        """.format(
            id=name
        )
        for item in spec.items["enum"]:
            tmpl += """<option value="{i}">{i}</option>
            """.format(
                i=item
            )
        tmpl += """  </select>
        </div>
        """
        return tmpl
    else:
        return """<input class="input" type="tags" placeholder="Add Tag" name="{}.default">""".format(
            name
        )


@app.route("/admin/settings")
def admin_settings():
    services = list(db.settings.find())
    return render_template("admin/settings.html.jinja", services=services)


@app.route("/admin/users")
def admin_users():
    services = list(db.users.find())
    return render_template("admin/users.html.jinja", services=services)
