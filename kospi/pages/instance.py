#!/usr/bin/env python3

from flask import render_template, request, url_for, redirect
from uuid import uuid4
from kospi import db, app


import pprint

pp = pprint.PrettyPrinter(indent=4)


@app.route("/")
def instance_list():
    instances = list(db.instance.find())
    for instance in instances:
        service = db.service.find_one(
            {"metadata.uuid": instance["metadata"]["service_uuid"]}
        )
        instance["service"] = service
    return render_template("instance_list.html.jinja", instances=instances)


@app.route("/instance/create")
def instance_create():
    services = list(db.service.find())
    return render_template("instance_create.html.jinja", services=services)


@app.route("/instance/delete/<uuid:uuid>")
def instance_delete(uuid):
    res = db.instance.delete_one({"metadata.uuid": uuid})
    return redirect(url_for("instance_list"))


@app.route("/instance/create/<uuid:uuid>", methods=["GET", "POST"])
def instance_create_uuid(uuid):
    if request.method == "GET":
        service = next(db.service.find({"metadata.uuid": uuid}), None)
        spec = next(
            filter(
                lambda ver: ver["name"] == service["metadata"]["version"],
                service["spec"]["spec"]["versions"],
            ),
            None,
        )
        tmpl = render_form(
            spec["schema"]["open_apiv3_schema"]["properties"]["spec"]["properties"]
        )
        return render_template(
            "instance_create_uuid.html.jinja", service=service, spec=spec, tmpl=tmpl
        )
    if request.method == "POST":
        data = parse_form(request.form)
        pp.pprint(data)
        instance = {
            "metadata": {
                "owner": "testuser",
                "uuid": uuid4(),
                "service_uuid": uuid,
                "name": data["metadata"]["name"],
                "description": data["metadata"]["description"],
            },
            "instance": data["payload"],
        }
        db.instance.insert_one(instance)
        return redirect(url_for("instance_list"))


def parse_form(data):
    res = {}
    for (key, value) in data.items():
        path = key.split(".")
        tmp = res
        for i, node in enumerate(path):
            if i == len(path) - 1:
                if value == "superuniqueboolvalue":
                    value = True
                tmp[node] = value
            else:
                if node not in tmp:
                    tmp[node] = {}
            tmp = tmp[node]
    return res


def render_form(spec, path="payload"):
    if spec is None:
        print("no item found {}".format(spec))
        return ""

    tmpl = ""
    for (key, value) in spec.items():
        if value.get("enabled", False):
            tmpl += """
            <div class="box">
                <div class="field">
            """
            if value["type"] == "string":
                tmpl += """
                <div class="control">
                    <label class="label">{name}</label>
                    <input class="input" type="text" name="{id}">
                    <p class="help">{help}</p>
                </div>
                """.format(
                    name=key, id=joins(path, key), help=value["description"]
                )
            elif value["type"] == "integer":
                tmpl += """
                <div class="control">
                    <label class="label">{name}</label>
                    <input class="input" type="number" name="{id}">
                    <p class="help">{help}</p>
                </div>
                """.format(
                    name=key, id=joins(path, key), help=value["description"]
                )
            elif value["type"] == "boolean":
                tmpl += """
                <div class="control">
                    <label class="label">
                    <input type="checkbox" name="{id}" value="superuniqueboolvalue">
                    {name}</label>
                    <p class="help">{help}</p>
                </div>
                """.format(
                    name=key, id=joins(path, key), help=value["description"]
                )
            elif value["type"] == "object":
                tmpl += """<label class="label">{}</label>""".format(key)
                tmpl += render_form(value.properties, joins(path, key))
            elif value["type"] == "array":
                tmpl += """<label class="label">{}</label>""".format(key)
                tmpl += render_form_array(key, value)
            tmpl += """  </div>
            </div>
            """
    return tmpl


def joins(*argv):
    res = ""
    for arg in argv:
        if arg.startswith("."):
            res += arg[1:] + "."
        elif arg.endswith("."):
            res += arg
        else:
            res += arg + "."
    if res.endswith("."):
        res = res[:-1]
    if res.startswith("."):
        res = res[1:]

    return res


def render_form_array(name, spec):
    if "enum" in spec["items"]:
        tmpl = """<div class="select is-multiple">
        <select multiple size="8" name="{id}" id="{id}">
        """.format(
            id=name
        )
        for item in spec["items"]["enum"]:
            tmpl += """<option value="{i}">{i}</option>
            """.format(
                i=item
            )
        tmpl += """  </select>
        </div>
        """
        return tmpl
    else:
        return """<input class="input" type="tags" placeholder="Add Tag" name="{}">""".format(
            name
        )


@app.route("/instance/edit/<uuid:uuid>")
def instance_edit(uuid):
    instance = list(db.instance.find({"metadata.uuid": uuid}))
    return render_template("instance_edit.html.jinja", instance=instance)
