#!/usr/bin/env python3

import toml


class Config:
    CONFIG = object


def get_config():
    Config.CONFIG = toml.load("config.toml")
