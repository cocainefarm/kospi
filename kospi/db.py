#!/usr/bin/env python3

from pymongo import MongoClient
from .config import Config

from urllib.parse import quote_plus


client = MongoClient(
    "mongodb://%s:%s@%s:%s/%s"
    % (
        quote_plus(Config.CONFIG["database"]["username"]),
        quote_plus(Config.CONFIG["database"]["password"]),
        Config.CONFIG["database"]["host"],
        Config.CONFIG["database"]["port"],
        Config.CONFIG["database"]["database"],
    )
)

db = client.kospi
