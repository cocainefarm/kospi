#!/usr/bin/env python3

from kubernetes import client, config

from .config import Config

# Configs can be set in Configuration class directly or using helper utility
config.load_kube_config()

apiext = client.ApiextensionsV1Api()


def get_custom_resources(name=None):
    if name == None:
        return apiext.list_custom_resource_definition(watch=False).items
    else:
        return apiext.read_custom_resource_definition(name=name)
