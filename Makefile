##
# KOSPI
#
# @file
# @version 0.1

.PHONY: default
default: kospi/static/style.css

.PHONY: run
run:
	FLASK_APP="kospi/__init__.py" FLASK_ENV="development" flask run

.PHONY: clean
clean:
	rm kospi/static/style.css

kospi/static/style.css: style/*
	sassc style/style.scss > kospi/static/style.css

# end
