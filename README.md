# KOSPI - Kubernetes Operator Service Provisioning Interface

## Goals
* Provide an interface to easily provision new Applications managed by kubernetes operators by submitting CustomResources to the kubernetes cluster.
* Allow Customers to buy and controll new services.
* Services are defined dynamically.
* provide user management
  * Different user permission levels for Admins, Customers, etc.
* user groups/organizations

* Backend written in python
  * using flask for web api server
  * mongodb or similar as database
  
* Frontend in vuejs

## Architecture

* New services can be created based on which CustomResourceDefinitions are present in the kubernetes cluster.
    * Default values, lists of possible choices, etc are associated with these
      fields. fields can be disabled.
    * Schema get stored in a `schema` datastore
      * These are taken on provisioning a new service, customize the values,
        store in `services` datastore

![kospi.png](./docs/kospi.png)


## Schema
### Service

``` json
{
    "metadata": {
        "cost": <>,
    }
    "id": string,
    "name": string,
    "crdref": string,
    "schema": {
        <Whole Custom Resource Definition>
    }
}
```

### Provisioned Services

``` json
{
    "metadata": {
        "owner": string,
        
    }
    "id": string,
    "name": string,
    "serviceType": string,
    "serviceSpec": {
        <Service Spec>
    },
}
```

