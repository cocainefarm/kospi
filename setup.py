#!/usr/bin/env python3

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="kospi", # Replace with your own username
    version="0.0.1",
    author="Max Audron",
    author_email="audron@cocaine.farm",
    description="Kubernetes Operator Service Provisioning Interface Backend",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cocainefarm/kospi",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
